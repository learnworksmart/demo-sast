# Include SAST to our GitLab CI/CD
This demo introduces SAST to our a CI/CD workflow.

![image](/uploads/84cadc5e6ffa45dd37d6c074bf780947/image.png)

## References: 
1. https://docs.gitlab.com/ee/user/application_security/sast/
